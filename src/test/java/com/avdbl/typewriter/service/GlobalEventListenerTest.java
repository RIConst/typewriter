package com.avdbl.typewriter.service;

import org.jnativehook.keyboard.NativeKeyEvent;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;

import static org.mockito.Mockito.*;

class GlobalEventListenerTest {

    @Test
    public void triggerEvent_writeFromTextInput1() {
        ExecutorService mockExecutorService = mock(ExecutorService.class);
        GlobalEventListener listener = new GlobalEventListener(mockExecutorService);

        NativeKeyEvent ctrlEvent = mock(NativeKeyEvent.class);
        when(ctrlEvent.getKeyCode()).thenReturn(0x001D);
        NativeKeyEvent oneEvent = mock(NativeKeyEvent.class);
        when(oneEvent.getKeyCode()).thenReturn(0x0002);

        listener.nativeKeyPressed(ctrlEvent);
        listener.nativeKeyPressed(oneEvent);

        verify(mockExecutorService, times(1)).submit(any(Runnable.class));
    }

    @Test
    public void triggerEvent_shutdown() {
        ExecutorService mockExecutorService = mock(ExecutorService.class);
        GlobalEventListener listener = new GlobalEventListener(mockExecutorService);

        NativeKeyEvent ctrlEvent = mock(NativeKeyEvent.class);
        when(ctrlEvent.getKeyCode()).thenReturn(0x001D);
        NativeKeyEvent shiftEvent = mock(NativeKeyEvent.class);
        when(shiftEvent.getKeyCode()).thenReturn(0x002A);
        NativeKeyEvent cEvent = mock(NativeKeyEvent.class);
        when(cEvent.getKeyCode()).thenReturn(0x002E);

        listener.nativeKeyPressed(ctrlEvent);
        listener.nativeKeyPressed(shiftEvent);
        listener.nativeKeyPressed(cEvent);

        verify(mockExecutorService, times(1)).shutdownNow();
    }

}