package com.avdbl.typewriter.writer;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.mockito.Mockito.*;

class TextWriterTest {

    @Test
    public void runTask_success() {
        final Consumer<Integer> mockConsumer = Mockito.mock(Consumer.class);
        final List<String> lines = new ArrayList<>();
        lines.add("Test1");
        lines.add("");
        lines.add("testing");
        final TextWriter task = new TextWriter(() -> lines, mockConsumer);

        task.run();
        // 12 chars (see above) + 2 end-of-line characters
        verify(mockConsumer, times(14)).accept(anyInt());
    }

}