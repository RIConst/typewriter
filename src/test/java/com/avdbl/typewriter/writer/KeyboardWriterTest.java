package com.avdbl.typewriter.writer;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class KeyboardWriterTest {

    @Test
    public void asciiToKeySequenceTest() {
        final List<Integer> lowerCaseCharacterSeq = KeyboardWriter.asciiToKeySequence('t');
        assertEquals(1, lowerCaseCharacterSeq.size());
        assertEquals(84, lowerCaseCharacterSeq.get(0));

        final List<Integer> upperCaseCharacterSeq = KeyboardWriter.asciiToKeySequence('T');
        assertEquals(2, upperCaseCharacterSeq.size());
        assertEquals(16, upperCaseCharacterSeq.get(0));
        assertEquals(84, upperCaseCharacterSeq.get(1));

        final List<Integer> numberCharacterSeq = KeyboardWriter.asciiToKeySequence('1');
        assertEquals(1, numberCharacterSeq.size());
        assertEquals(49, numberCharacterSeq.get(0));
    }

    @Test
    public void typeCharacter() {
        final Robot mockRobot = Mockito.mock(Robot.class);

        final List<Integer> lowerCaseCharacterSeq = Arrays.asList(84); // t
        KeyboardWriter.typeKeys(lowerCaseCharacterSeq, mockRobot);

        final List<Integer> upperCaseCharacterSeq = Arrays.asList(16, 84); // T
        KeyboardWriter.typeKeys(upperCaseCharacterSeq, mockRobot);

        final List<Integer> numberCharacterSeq = Arrays.asList(49); // 1
        KeyboardWriter.typeKeys(numberCharacterSeq, mockRobot);

        verify(mockRobot, times(2)).keyPress(84); //t
        verify(mockRobot, times(1)).keyPress(16); //shift
        verify(mockRobot, times(1)).keyPress(49); //1
        verify(mockRobot, times(2)).keyRelease(84); //t
        verify(mockRobot, times(1)).keyRelease(16); //shift
        verify(mockRobot, times(1)).keyRelease(49); //1
    }
}