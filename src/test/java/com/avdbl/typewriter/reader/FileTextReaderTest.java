package com.avdbl.typewriter.reader;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FileTextReaderTest {

    @Test
    public void readFile_success() {
        List<String> testLines = new ArrayList<>();
        testLines.add("test");
        testLines.add("testing");
        testLines.add("");
        testLines.add("tester");
        BufferedReader mockedReader = mock(BufferedReader.class);
        when(mockedReader.lines()). thenReturn(testLines.stream());

        List<String> output = TextReader.lineSupplier.apply(() -> mockedReader).get();

        assertNotNull(output);
        assertEquals(4, output.size());
        assertEquals("test", output.get(0));
        assertEquals("testing", output.get(1));
        assertEquals("", output.get(2));
        assertEquals("tester", output.get(3));
    }
}