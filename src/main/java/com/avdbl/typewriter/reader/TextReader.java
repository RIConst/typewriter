package com.avdbl.typewriter.reader;

import java.io.BufferedReader;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public abstract class TextReader {

    // Takes in a supplier of a BufferedReader and returns a supplier of lines of text (on an
    // empty list if readerSupplier is null)
    public static final Function<Supplier<BufferedReader>, Supplier<List<String>>> lineSupplier =
            (readerSupplier) -> () -> Optional.of(readerSupplier)
                    .map(Supplier::get)
                    .map(BufferedReader::lines)
                    .map(s -> s.collect(Collectors.toList()))
                    .orElseGet(Collections::emptyList);
}
