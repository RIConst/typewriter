package com.avdbl.typewriter.service;

import com.avdbl.typewriter.reader.TextReader;
import com.avdbl.typewriter.writer.KeyboardWriter;
import com.avdbl.typewriter.writer.TextWriter;
import io.vavr.control.Try;
import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;

import static io.vavr.API.*;
import static io.vavr.Predicates.is;

public class GlobalEventListener extends GlobalScreen implements NativeKeyListener {

    private static final String INPUT_FILE_PATTERN = "input/text_input_%d";
    private static final int NATIVE_KEY_EVENT_NUMBER_OFFSET = 1;

    private static final List<TextWriter> tasks = new ArrayList<>();
    private static final Set<Integer> keysHeld = new HashSet<>();

    private final ExecutorService executorService;

    public GlobalEventListener(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent e) {
        keysHeld.add(e.getKeyCode());
        // Using Match and Case instead of regular switch statement - Provide a condition and action
        // In this case, the action is a consumer of key codes (written as a supplier of consumer of integer)
        Match(e.getKeyCode()).of(
                Case($(between1To9AndControlIsPressed::apply), () -> (Consumer<Integer>) this::createWriterTask),
                Case($(is(NativeKeyEvent.VC_ESCAPE)), () -> keyCode -> stopTasks()),
                Case($(is(NativeKeyEvent.VC_F7)), () -> keyCode -> pauseTasks()),
                Case($(ctrlShiftCIsPressed::apply), () -> keyCode -> shutDownTasks()),
                Case($(), () -> keyCode -> {/*no-op*/})
        ).accept(e.getKeyCode() - NATIVE_KEY_EVENT_NUMBER_OFFSET);
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent e) {
        keysHeld.remove(e.getKeyCode());
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent e) {
        //no-op
    }

    public static void setLogLever(Level level) {
        log.setLevel(level);
    }

    private void createWriterTask(int fileNumber) {
        Try.of(() -> new FileReader(String.format(INPUT_FILE_PATTERN, fileNumber)))
                .map(BufferedReader::new)
                .map(br -> new TextWriter(TextReader.lineSupplier.apply(() -> br), KeyboardWriter.charConsumer))
                .onFailure(failure -> System.out.println("error: " + failure))
                .onSuccess(tasks::add)
                .onSuccess(executorService::submit);
    }

    private static void stopTasks() {
        tasks.forEach(TextWriter::stop);
        tasks.clear();
    }

    private static void pauseTasks() {
        tasks.forEach(TextWriter::togglePause);
    }

    private void shutDownTasks() {
        stopTasks();
        executorService.shutdownNow();
        Try.run(GlobalEventListener::unregisterNativeHook);
    }

    private static final Function<Integer, Boolean> between1To9AndControlIsPressed =
            n -> NativeKeyEvent.VC_1 <= n &&
                    NativeKeyEvent.VC_9 >= n &&
                    keysHeld.contains(NativeKeyEvent.VC_CONTROL);

    private static final Function<Integer, Boolean> ctrlShiftCIsPressed =
            n -> NativeKeyEvent.VC_C == n &&
                    keysHeld.contains(NativeKeyEvent.VC_CONTROL) &&
                    keysHeld.contains(NativeKeyEvent.VC_SHIFT);
}
