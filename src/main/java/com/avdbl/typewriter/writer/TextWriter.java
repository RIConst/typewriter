package com.avdbl.typewriter.writer;

import io.vavr.collection.Stream;
import io.vavr.control.Try;

import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class TextWriter implements Runnable {

    private final Supplier<List<String>> linesSupplier;
    private final Consumer<Integer> charConsumer;

    private volatile boolean stop = false;
    private volatile boolean pause = false;

    private final Semaphore charSemaphore;

    public TextWriter(Supplier<List<String>> linesSupplier, Consumer<Integer> charConsumer) {
        this.linesSupplier = linesSupplier;
        this.charConsumer = charConsumer;
        this.charSemaphore = new Semaphore(1);
    }

    @Override
    public void run() {
        Try.run(() ->Thread.sleep(500));
        writeLines(linesSupplier.get(), charConsumer);
    }

    public void stop() {
        stop = true;
        releaseLock(charSemaphore);
    }

    public synchronized void togglePause() {
        pause = !pause;
        if (pause) {
            acquireLock(charSemaphore);
        } else {
            releaseLock(charSemaphore);
        }
    }

    // Common pattern of looping through lines and writing out characters
    private void writeLines(List<String> lines, Consumer<Integer> charConsumer) {
        Stream.ofAll(lines)
                .filter(l -> !stop)
                .map(l -> l.replaceFirst("\\s*", ""))
                .zipWithIndex()
                .forEach(tuple -> {
                    tuple._1().chars()
                            .filter(charCode -> !stop)
                            // Impure code gets executed here
                            .forEachOrdered(this::writeChar);
                    // is char at end of line?
                    if (tuple._2() != lines.size() - 1) {
                        // And here
                        charConsumer.accept((int)'\n');
                    }
                });
    }

    private void writeChar(int charCode) {
        acquireLock(charSemaphore);
        charConsumer.accept(charCode);
        releaseLock(charSemaphore);
    }

    private void acquireLock(Semaphore semaphore) {
        Try.run(semaphore::acquire);
    }

    private void releaseLock(Semaphore semaphore) {
        semaphore.release();
        Try.run(() -> Thread.sleep(1));
    }
}
