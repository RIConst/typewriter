package com.avdbl.typewriter.writer;

import io.vavr.control.Try;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Map;
import java.util.*;
import java.util.function.Consumer;

import static io.vavr.API.*;

public class KeyboardWriter {

    private static final int MAX_DELAY_MILLIS = 20;
    private static final int MIN_DELAY_MILLIS = 10;
    private static final Map<Integer, List<Integer>> robotSpecialCharMap = new HashMap<>();

    private static final int NEW_LINE = 10;
    private static final int CARRIAGE_RETURN = 13;
    private static final int SPACE = 32;
    private static final int EXCLAMATION_MARK = 33;
    private static final int DOUBLE_QUOTES = 34;
    private static final int HASH = 35;
    private static final int DOLLAR = 36;
    private static final int PERCENT = 37;
    private static final int AMPERSAND = 38;
    private static final int SINGLE_QUOTE = 39;
    private static final int LEFT_PARENTHESIS = 40;
    private static final int RIGHT_PARENTHESIS = 41;
    private static final int ASTERISK = 42;
    private static final int PLUS = 43;
    private static final int COMMA = 44;
    private static final int MINUS = 45;
    private static final int PERIOD = 46;
    private static final int FORWARD_SLASH = 47;
    private static final int ZERO = 48;
    private static final int ONE = 49;
    private static final int TWO = 50;
    private static final int THREE = 51;
    private static final int FOUR = 52;
    private static final int FIVE = 53;
    private static final int SIX = 54;
    private static final int SEVEN = 55;
    private static final int EIGHT = 56;
    private static final int NINE = 57;
    private static final int COLON = 58;
    private static final int SEMI_COLON = 59;
    private static final int LESS_THAN = 60;
    private static final int EQUALS = 61;
    private static final int GREATER_THAN = 62;
    private static final int QUESTION_MARK = 63;
    private static final int AT = 64;
    private static final int OPEN_SQUARE_BRACKET = 91;
    private static final int BACK_SLASH = 92;
    private static final int CLOSE_SQUARE_BRACKET = 93;
    private static final int CARROT = 94;
    private static final int UNDERSCORE = 95;
    private static final int BACK_QUOTE = 96;
    private static final int OPEN_BRACE = 123;
    private static final int BAR = 124;
    private static final int CLOSE_BRACE = 125;
    private static final int TILDE = 126;

    public static void createSpecialCharacterMap() {
        robotSpecialCharMap.put(NEW_LINE, Arrays.asList(KeyEvent.VK_ENTER));
        robotSpecialCharMap.put(CARRIAGE_RETURN, Arrays.asList(KeyEvent.VK_ENTER));
        robotSpecialCharMap.put(SPACE, Arrays.asList(KeyEvent.VK_SPACE));
        robotSpecialCharMap.put(EXCLAMATION_MARK, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_1));
        robotSpecialCharMap.put(DOUBLE_QUOTES,	Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_QUOTE));
        robotSpecialCharMap.put(HASH, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_3));
        robotSpecialCharMap.put(DOLLAR,	Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_4));
        robotSpecialCharMap.put(PERCENT, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_5));
        robotSpecialCharMap.put(AMPERSAND, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_7));
        robotSpecialCharMap.put(SINGLE_QUOTE, Arrays.asList(KeyEvent.VK_QUOTE));
        robotSpecialCharMap.put(LEFT_PARENTHESIS, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_9));
        robotSpecialCharMap.put(RIGHT_PARENTHESIS, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_0));
        robotSpecialCharMap.put(ASTERISK, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_8));
        robotSpecialCharMap.put(PLUS, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_EQUALS));
        robotSpecialCharMap.put(COMMA, Arrays.asList(KeyEvent.VK_COMMA));
        robotSpecialCharMap.put(MINUS, Arrays.asList(KeyEvent.VK_MINUS));
        robotSpecialCharMap.put(PERIOD,	Arrays.asList(KeyEvent.VK_PERIOD));
        robotSpecialCharMap.put(FORWARD_SLASH, Arrays.asList(KeyEvent.VK_SLASH));
        robotSpecialCharMap.put(ZERO, Arrays.asList(KeyEvent.VK_0));
        robotSpecialCharMap.put(ONE, Arrays.asList(KeyEvent.VK_1));
        robotSpecialCharMap.put(TWO, Arrays.asList(KeyEvent.VK_2));
        robotSpecialCharMap.put(THREE, Arrays.asList(KeyEvent.VK_3));
        robotSpecialCharMap.put(FOUR, Arrays.asList(KeyEvent.VK_4));
        robotSpecialCharMap.put(FIVE, Arrays.asList(KeyEvent.VK_5));
        robotSpecialCharMap.put(SIX, Arrays.asList(KeyEvent.VK_6));
        robotSpecialCharMap.put(SEVEN, Arrays.asList(KeyEvent.VK_7));
        robotSpecialCharMap.put(EIGHT, Arrays.asList(KeyEvent.VK_8));
        robotSpecialCharMap.put(NINE, Arrays.asList(KeyEvent.VK_9));
        robotSpecialCharMap.put(COLON, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_SEMICOLON));
        robotSpecialCharMap.put(SEMI_COLON,	Arrays.asList(KeyEvent.VK_SEMICOLON));
        robotSpecialCharMap.put(LESS_THAN, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_COMMA));
        robotSpecialCharMap.put(EQUALS,	Arrays.asList(KeyEvent.VK_EQUALS));
        robotSpecialCharMap.put(GREATER_THAN, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_PERIOD));
        robotSpecialCharMap.put(QUESTION_MARK, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_SLASH));
        robotSpecialCharMap.put(AT,	Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_2));
        robotSpecialCharMap.put(OPEN_SQUARE_BRACKET, Arrays.asList(KeyEvent.VK_OPEN_BRACKET));
        robotSpecialCharMap.put(BACK_SLASH,	Arrays.asList(KeyEvent.VK_BACK_SLASH));
        robotSpecialCharMap.put(CLOSE_SQUARE_BRACKET, Arrays.asList(KeyEvent.VK_CLOSE_BRACKET));
        robotSpecialCharMap.put(CARROT,	Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_6));
        robotSpecialCharMap.put(UNDERSCORE,	Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_MINUS));
        robotSpecialCharMap.put(BACK_QUOTE,	Arrays.asList(KeyEvent.VK_BACK_QUOTE));
        robotSpecialCharMap.put(OPEN_BRACE, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_OPEN_BRACKET));
        robotSpecialCharMap.put(BAR, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_BACK_SLASH));
        robotSpecialCharMap.put(CLOSE_BRACE, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_CLOSE_BRACKET));
        robotSpecialCharMap.put(TILDE, Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_BACK_QUOTE));
    }

    static {
        createSpecialCharacterMap();
    }

    // A Consumer of integer - takes in a charCode (integer rep of each character
    // of text) and types it out using the Robot library.
    public static Consumer<Integer> charConsumer =
            charCode -> Try.of(() -> charCode)
                    .map(KeyboardWriter::asciiToKeySequence)
                    .onSuccess(keysToPress -> Try.of(Robot::new)
                            .onSuccess(robot -> typeKeys(keysToPress, robot))
                            .onFailure(ex -> System.out.println("log exception here: " + ex)))
                    .onFailure(ex -> System.out.println("log exception here: " + ex));

    // ==============================================================
    // ============ Impure Private/Protected Methods ================
    // ==============================================================

    protected static void typeKeys(List<Integer> keysToType, Robot robot) {
        keysToType.forEach(keyToPress -> pressKey(keyToPress, robot));
        keysToType.forEach(keyToRelease -> releaseKey(keyToRelease, robot));
    }

    private static void pressKey(int keyToPress, Robot robot) {
        robot.keyPress(keyToPress);
        robot.delay(randomDelay());
    }

    private static void releaseKey(int keyToRelease, Robot robot) {
        robot.keyRelease(keyToRelease);
        robot.delay(randomDelay());
    }

    private static int randomDelay() {
        final Random rand = new Random();
        if (rand.nextInt(31) == 1) {
            return 100;
        }
        return new Random().nextInt(MAX_DELAY_MILLIS - MIN_DELAY_MILLIS + 1) + MIN_DELAY_MILLIS;
    }

    // ==============================================================
    // ============= Pure Private/Protected Methods =================
    // ==============================================================

    // Map ascii char to a code (or sequence of codes) that Robot understands.
    // For example; when typing capital letters, shift needs to be pressed before
    // typing the character.
    protected static List<Integer> asciiToKeySequence(int charCode) {
        return Match(charCode).of(
                Case($(c -> c >= 65 && c <= 90), Arrays.asList(KeyEvent.VK_SHIFT, charCode)),
                Case($(c -> c >= 96 && c <= 122), Arrays.asList(charCode - 32)),
                Case($(), robotSpecialCharMap.get(charCode))
        );
    }
}
