package com.avdbl.typewriter;

import com.avdbl.typewriter.service.GlobalEventListener;
import io.vavr.control.Try;

import java.util.concurrent.Executors;
import java.util.logging.Level;

public class TypewriterApplication {

	public static void main(String[] args) {
		GlobalEventListener.setLogLever(Level.WARNING);
		Try.run(GlobalEventListener::registerNativeHook)
				.onFailure(TypewriterApplication::printExceptionAndExit);
		GlobalEventListener.addNativeKeyListener(new GlobalEventListener(Executors.newSingleThreadExecutor()));
	}

	private static void printExceptionAndExit(Throwable ex) {
		System.err.println("There was a problem registering the native hook.");
		System.err.println(ex.getMessage());
		System.exit(1);
	}
}
